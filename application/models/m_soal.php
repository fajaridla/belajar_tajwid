<?php

class M_soal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //function get_soal($limit, $id, $ket) {
    function get_soal() {
        $this->db->select('*');
        $this->db->from('soal');
//        $this->db->join('tajwid', 'soal.ID_TAJWID=tajwid.ID_TAJWID');
//        $this->db->where('KET_TAJWID', $ket);
//        $this->db->where('soal.ID_TAJWID', $id);
//        $this->db->limit($limit);

        $ambil = $this->db->get();
        if ($ambil->num_rows() > 0) {
            foreach ($ambil->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

//    function get_soal() {
//            $this->db->select('*');
//        $this->db->from('soal');
//        $this->db->join('tajwid','soal.ID_TAJWID=tajwid.ID_TAJWID');
//        $this->db->where('KET_TAJWID','makharijal');
//        $this->db->where('soal.ID_TAJWID','T01');
//        $this->db->limit(1);
//
//        $ambil = $this->db->get();
//        if ($ambil->num_rows() > 0) {
//            foreach ($ambil->result() as $data) {
//                $hasil[] = $data;
//            }
//            return $hasil;
//        }
//    }

    public function check_jawaban($id, $jawaban) {
        $this->db->select('*');
        $this->db->from('soal');
        $this->db->where('soal.ID_SOAL', $id);
        $this->db->where('soal.KUNCI_JAWABAN', $jawaban);
        $ambil = $this->db->get();
        return $ambil->result();
    }

}

?>
