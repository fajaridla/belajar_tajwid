<?php

class M_user_online extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        //    session_start();
    }

    function get_member_online($tanggal) {

        $this->db->select('*');
        $this->db->from('member_login');
        $this->db->where('date', $tanggal);
        $ambil = $this->db->get();

        if ($ambil->num_rows() < 1) {
            $insert_data = array(
                'jml_pengunjung' => 1,
                'date' => $tanggal);

            $insert = $this->db->insert('member_login', $insert_data);
            return $insert;
        } else {

            $data_member_login = array_shift($ambil->result_array());
            $jml_pengunjung = $data_member_login['jml_pengunjung'];

            $data = array(
                'jml_pengunjung' => $jml_pengunjung + 1,
                'date' => $tanggal
            );

            $this->db->where('date', $tanggal);
            $update = $this->db->update('member_login ', $data);
            return $update;
        }
    }
    
    public function get_total_pengunjung(){
        $this->db->select('jml_pengunjung');
        $this->db->from('member_login');  
        $this->db->order_by('no','desc');

        $query = $this->db->get();
        
        $array_jml_pengunjung = array();
        foreach ($query->result_array() as $row)
        {
            $array_jml_pengunjung [] = intval($row['jml_pengunjung']);
            
        }
        $total = array_sum($array_jml_pengunjung);
        
        //$this->db->where('jml_pengunjung',2);        
//        $array_total = $this->db->get()->result_array();
//        $total= array_sum($array_total);
          return $total;
          
    }
            

}

?>
