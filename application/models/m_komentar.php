<?php

class M_komentar extends CI_Model {
       function __construct() {
        parent::__construct();
        $this->load->helper('date');
    }

    function insert_komentar() {
        $datestring = "%d/%m/%Y ";
        $time = time();
        $mdate = mdate($datestring,$time);
        $insert_data = array(
            'ISI_KOMENTAR' => $this->input->post('komentar'),
            'TGL_POSTING' => $mdate);

        $insert = $this->db->insert('KOMENTAR', $insert_data);
        return $insert;
    }

}

?>
