<?php

Class M_member extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function cek_member() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $this->db->where('NM_MEMBER', $username);
        $this->db->where('PASS_MEMBER', $password);
        $query = $this->db->get('MEMBER');
        return $query->result();
    }

    function insert_member() {

        $new_member_insert_data = array(
            'EMAIL_MEMBER' => $this->input->post('email'),
            'ALMT_MEMBER' => $this->input->post('alamat'),
            'NM_MEMBER' => $this->input->post('username'),
            'PASS_MEMBER' => md5($this->input->post('password')),
        );

        $insert = $this->db->insert('MEMBER', $new_member_insert_data);
        return $insert;
    }

}

?>
