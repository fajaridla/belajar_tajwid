<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_user extends CI_Controller {
    
     function index(){
		$this->load->view('home_user');
              
	}

    public function navigasi($id) {
        switch ($id) {
            case 'home':
                $this->load->view('welcome');
                break;

            case 'materi_pengantar':
                $this->load->model('m_materi');
                $id_tajwid='T01';
                $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                $this->load->view("content/makharijal", $data);
                
                break;

            case 'komentar':
                $this->load->view('form_komentar_user');
                break;
        }
    }

}

