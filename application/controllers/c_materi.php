<?php

//

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_materi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        session_start();
        $this->username = $this->session->userdata('username');
    }

    function hijaiyah($side) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($side) {
                case 'makharijal':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T01';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/makharijal", $data);
                    break;

                case 'penjelasan_makharijal':
                    $this->load->view("content/mengeluarkan_huruf");
                    break;
            }
            //}
        } else {

            $this->session->sess_destroy();
            $this->load->view('home_user.php');
        }
//        else {
//            $this->load->view("home_user.php");
//        }
    }

    function nun_mati($side) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($side) {
                case 'idzhar':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T02';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idzhar", $data);
                    break;

                case 'idghom':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T03';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idghom", $data);
                    break;

                case 'ikhfa':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T04';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/ikhfa", $data);
                    break;

                case 'iqlab':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T05';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/iqlab", $data);
                    break;
            }
        } else {

            $this->session->sess_destroy();
            $this->load->view('home_user.php');
        }
    }

    function mim_mati($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'ikhfa_syafawi':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T06';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/ikhfa_syafawi", $data);
                    break;

                case 'idghom_mitslain':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T07';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idghom_mitslain", $data);
                    break;

                case 'idzhar_syafawi':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T08';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idzhar_syafawi", $data);
                    break;
            }
        } else {

            $this->session->sess_destroy();
            $this->load->view('home_user.php');
        }
    }

    function lam_tarif($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'alif_lam_syamsyiyah':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T14';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/alif_lam_syamsyiyah", $data);
                    break;

                case 'alif_lam_qomariyah':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T13';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/alif_lam_qomariyah", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function ra($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'tafkhim':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T16';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/tafkhim", $data);
                    break;

                case 'tarqiq':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T15';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/tarqiq", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function lam_jalalah($side) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($side) {
                case 'taglidzh':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T17';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/lam_jalalah", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function qolqolah($side) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($side) {
                case 'qolqolah_kubro_shugro':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T18';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/qolqolah", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
         
        }
    }

    function mad($side) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($side) {
                case 'mad_tobiiy':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T19';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/mad", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function surat($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'al-ikhlas':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T25';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/alikhlas", $data);
                    break;

                case 'al-falaq':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T24';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/alfalaq", $data);
                    break;

                case 'an-nas':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T23';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/annas", $data);
                    break;

                case 'al-masad':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T26';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/almasad", $data);
                    break;

                case 'an-nashr':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T27';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/annashr", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function hukum_idghom($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'idghom_mutamatsilaini';
                    $this->load->model('m_materi');
                    $id_tajwid = 'T09';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view('content/idghom_mutamatsilain', $data);
                    break;


                case 'idghom_mutajanisaini';
                    $this->load->model('m_materi');
                    $id_tajwid = 'T10';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view('content/idghom_mutajanisaini', $data);
                    break;


                case 'idghom_mutaqoribaini';
                    $this->load->model('m_materi');
                    $id_tajwid = 'T11';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view('content/idghom_mutaqoribaini', $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function istilah_quran($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'saktah';
                    $this->load->model('m_materi');
                    $id_tajwid = 'T21';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view('content/saktah', $data);
                    break;

                case 'sajadah';
                    $this->load->view('content/sajadah');
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    function jenis_waqof($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'jenis_waqof';
                    $this->load->model('m_materi');
                    $id_tajwid = 'T20';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view('content/waqof', $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

    public function tajwid_warna($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'warna_idzhar':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T02';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idzhar", $data);
                    break;


                case 'warna_idghom':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T03';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idghom", $data);
                    break;

                case 'warna_qolqolah':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T18';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/qolqolah", $data);
                    break;

                case 'warna_mad':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T19';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/mad", $data);
                    break;

                case 'warna_idzharSyafawi':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T08';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/idzhar_syafawi", $data);
                    break;

                case 'warna_syamsiyah':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T13';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/alif_lam_syamsyiyah", $data);
                    break;

                case 'warna_qomariyah':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T14';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/alif_lam_qomariyah", $data);
                    break;

                case 'warna_ikhfa':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T04';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view("content/ikhfa", $data);
                    break;
            }
        } else {
            $this->session->sess_destroy();
            $this->load->view('home_user.php');
            
        }
    }

}

