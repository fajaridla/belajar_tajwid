<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_admin_manage extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        session_start();
        $this->name_admin = $this->session->userdata('name_admin');
        $this->load->library('grocery_CRUD');
        $this->load->model('m_user_online');
    }

    public function index() {
//$this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
        $this->name_admin = $this->session->userdata('name_admin');
    }

    public function total_pengunjung($output = null) {


        $data['name_admin'] = $this->name_admin;

        $pengunjung['total'] = $this->m_user_online->get_total_pengunjung();
        

        $this->load->view('admin/header.php', $data);
        $this->load->view('admin/home_admin.php', $output);
        $this->load->view('admin/total.php', $pengunjung);
        //print_r($pengunjung);
        //print_r($pengunjung);
    }

    public function _example_output($output = null) {

//        $output['output'] = $output;
        $data['name_admin'] = $this->name_admin;

//
//
        $view = 'admin/home_admin.php';
        $dd = 'admin/header.php';
        //$this->load->view($view, $data);  
        //$data['header'] =    ;
        //$this->load->view('admin/header', $data);
        //$this->load->view($view, $data);
        $this->load->view($dd, $data);
        $this->load->view($view, $output);
        //$this->load->view($view, $data);  
    }

    public function offices() {
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    public function matters_management() {
        if ($this->session->userdata('is_logged_in') == TRUE) {
            try {
                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('materi');
//			//$crud->set_relation('ID_ADMIN','admin','NM_ADMIN');
                $crud->set_relation('ID_TAJWID', 'tajwid', 'KET_TAJWID');

                $crud->unset_columns('ID_ADMIN');
                $crud->unset_edit_fields('ID_ADMIN');
                $crud->unset_add_fields('ID_ADMIN');

                //$crud->display_as('ID_ADMIN','Nama Admin')
                $crud->display_as('ID_TAJWID', 'JENIS MATERI');
                //$crud->display_as('ID_TAJWID','Jenis Materi');


                $crud->set_subject('Materi');
                $crud->required_fields('ID_TAJWID', 'LATIN', 'SUARA');
                $crud->set_field_upload('HIJAIYAH_IMG', 'assets/img_hijaiyah');
                $crud->set_field_upload('SUARA', 'assets/sound_materi');
                //$crud->unset_fields('ID_ADMIN');
                //$crud->required_fields('BUTIR_SOAL','ID_ADMIN');
                //$crud->set_field_upload('SOUND_SOAL','sound/soal');
//                $dta =(object) array ($crud->render());
//                $data['username'] = $this->session->userdata('username');
//           
                $output = $crud->render();
//                $data = array();
//                $data['username']=$this->session->userdata('username');
//                $output->data = $data;
//                
                $this->_example_output($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        } else {
            $this->load->view('admin/form_admin');
        }
    }

    public function question_management() {

        if ($this->session->userdata('is_logged_in') == TRUE) {
            try {
                $crud = new grocery_CRUD();
                $crud->set_theme('datatables');
                $crud->set_table('soal');
                //$crud->set_relation('ID_TAJWID','tajwid','KET_TAJWID');

                $crud->set_relation('ID_TAJWID', 'tajwid', 'KET_TAJWID');

                $crud->unset_columns('ID_ADMIN');
                $crud->unset_edit_fields('ID_ADMIN');
                $crud->unset_add_fields('ID_ADMIN');
                $crud->display_as('ID_TAJWID', 'JENIS MATERI');
                //$crud->display_as('ID_ADMIN','Nama Admin')->display_as('ID_TAJWID','Jenis Materi');
                //$crud->display_as('ID_TAJWID','Jenis Materi');


                $crud->set_subject('Soal');

                //$crud->unset_fields('ID_ADMIN');
                $crud->required_fields('BUTIR_SOAL', 'ID_ADMIN', 'ID_TAJWID', 'KUNCI_JAWABAN', 'JAWABAN_A', 'JAWABAN_B', 'JAWABAN_C', 'JAWABAN_D');
                //      $crud->required_fields();



                $crud->set_field_upload('SOUND_SOAL', 'assets/files_soal');


                $output = $crud->render();

                $this->_example_output($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        } else {
            $this->load->view('admin/form_admin');
        }
    }

    public function info_management() {

        if ($this->session->userdata('is_logged_in') == TRUE) {
            try {
                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('info');
                
//			//$crud->set_relation('ID_ADMIN','admin','NM_ADMIN');
//			$crud->set_relation('ID_TAJWID','tajwid','KET_TAJWID');
                $crud->unset_columns('ID_ADMIN');
                $crud->unset_edit_fields('ID_ADMIN');
                $crud->unset_add_fields('ID_ADMIN');
              

                //$crud->display_as('ID_ADMIN','Nama Admin')->display_as('ID_TAJWID','Jenis Materi');
                //$crud->display_as('ID_TAJWID','Jenis Materi');


                $crud->set_subject('Info');
                $crud->required_fields('INFO', 'TGL_INPUT_INFO');
                $crud->order_by('id_info','desc');
                //$crud->unset_fields('ID_ADMIN');
                //$crud->required_fields('BUTIR_SOAL','ID_ADMIN');
                //$crud->set_field_upload('SOUND_SOAL','sound/soal');

                $output = $crud->render();
                $this->_example_output($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        } else {
            $this->load->view('admin/form_admin');
        }
    }

    public function comment_management() {

        if ($this->session->userdata('is_logged_in') == TRUE) {
            try {
                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('komentar');
//			//$crud->set_relation('ID_ADMIN','admin','NM_ADMIN');
//			$crud->set_relation('ID_TAJWID','tajwid','KET_TAJWID');
                $crud->unset_columns('ID_MEMBER');
                $crud->unset_edit_fields('ID_MEMBER');
                $crud->unset_add_fields('ID_MEMBER');

                //$crud->display_as('ID_ADMIN','Nama Admin')->display_as('ID_TAJWID','Jenis Materi');
                //$crud->display_as('ID_TAJWID','Jenis Materi');
                $crud->unset_edit();
                $crud->unset_add();

                $crud->set_subject('Komentar');


                //$crud->unset_fields('ID_ADMIN');
                //$crud->required_fields('BUTIR_SOAL','ID_ADMIN');
                //$crud->set_field_upload('SOUND_SOAL','sound/soal');



                $output = $crud->render();
                $this->_example_output($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        } else {
            $this->load->view('admin/form_admin');
        }
    }

    public function visitor_counter() {

        if ($this->session->userdata('is_logged_in') == TRUE) {
            try {
                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('member_login');
//			//$crud->set_relation('ID_ADMIN','admin','NM_ADMIN');
//			$crud->set_relation('ID_TAJWID','tajwid','KET_TAJWID');
//                $crud->unset_columns('ID_MEMBER');
//                $crud->unset_edit_fields('ID_MEMBER');
//                $crud->unset_add_fields('ID_MEMBER');
                //$crud->display_as('ID_ADMIN','Nama Admin')->display_as('ID_TAJWID','Jenis Materi');
                //$crud->display_as('ID_TAJWID','Jenis Materi');
                $crud->unset_edit();
                $crud->unset_add();

                $crud->set_subject('member_login');


                //$crud->unset_fields('ID_ADMIN');
                //$crud->required_fields('BUTIR_SOAL','ID_ADMIN');
                //$crud->set_field_upload('SOUND_SOAL','sound/soal');



                $output = $crud->render();
                $this->total_pengunjung($output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        } else {
            $this->load->view('admin/form_admin');
        }
    }

    public function products_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('products');
        $crud->set_subject('Product');
        $crud->unset_columns('productDescription');
        $crud->callback_column('buyPrice', array($this, 'valueToEuro'));

        $output = $crud->render();

        $this->_example_output($output);
    }

    public function valueToEuro($value, $row) {
        return $value . ' &euro;';
    }

    public function film_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('film');
        $crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname', 'priority');
        $crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
        $crud->unset_columns('special_features', 'description', 'actors');

        $crud->fields('title', 'description', 'actors', 'category', 'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

        $output = $crud->render();

        $this->_example_output($output);
    }

    public function film_management_twitter_bootstrap() {
        try {
            $crud = new grocery_CRUD();

            $crud->set_theme('twitter-bootstrap');
            $crud->set_table('film');
            $crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname', 'priority');
            $crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
            $crud->unset_columns('special_features', 'description', 'actors');

            $crud->fields('title', 'description', 'actors', 'category', 'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

            $output = $crud->render();
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    function multigrids() {
        $this->config->load('grocery_crud');
        $this->config->set_item('grocery_crud_dialog_forms', true);
        $this->config->set_item('grocery_crud_default_per_page', 10);

        $output1 = $this->offices_management2();

        $output2 = $this->employees_management2();

        $output3 = $this->customers_management2();

        $js_files = $output1->js_files + $output2->js_files + $output3->js_files;
        $css_files = $output1->css_files + $output2->css_files + $output3->css_files;
        $output = "<h1>List 1</h1>" . $output1->output . "<h1>List 2</h1>" . $output2->output . "<h1>List 3</h1>" . $output3->output;

        $this->_example_output((object) array(
                    'js_files' => $js_files,
                    'css_files' => $css_files,
                    'output' => $output
        ));
    }

    public function offices_management2() {
        $crud = new grocery_CRUD();
        $crud->set_table('offices');
        $crud->set_subject('Office');

        $crud->set_crud_url_path(site_url(strtolower(__CLASS__ . "/" . __FUNCTION__)), site_url(strtolower(__CLASS__ . "/multigrids")));

        $output = $crud->render();

        if ($crud->getState() != 'list') {
            $this->_example_output($output);
        } else {
            return $output;
        }
    }

    public function employees_management2() {
        $crud = new grocery_CRUD();

        $crud->set_theme('datatables');
        $crud->set_table('employees');
        $crud->set_relation('officeCode', 'offices', 'city');
        $crud->display_as('officeCode', 'Office City');
        $crud->set_subject('Employee');

        $crud->required_fields('lastName');

        $crud->set_field_upload('file_url', 'assets/uploads/files');

        $crud->set_crud_url_path(site_url(strtolower(__CLASS__ . "/" . __FUNCTION__)), site_url(strtolower(__CLASS__ . "/multigrids")));

        $output = $crud->render();

        if ($crud->getState() != 'list') {
            $this->_example_output($output);
        } else {
            return $output;
        }
    }

    public function customers_management2() {

        $crud = new grocery_CRUD();

        $crud->set_table('customers');
        $crud->columns('customerName', 'contactLastName', 'phone', 'city', 'country', 'salesRepEmployeeNumber', 'creditLimit');
        $crud->display_as('salesRepEmployeeNumber', 'from Employeer')
                ->display_as('customerName', 'Name')
                ->display_as('contactLastName', 'Last Name');
        $crud->set_subject('Customer');
        $crud->set_relation('salesRepEmployeeNumber', 'employees', 'lastName');

        $crud->set_crud_url_path(site_url(strtolower(__CLASS__ . "/" . __FUNCTION__)), site_url(strtolower(__CLASS__ . "/multigrids")));

        $output = $crud->render();

        if ($crud->getState() != 'list') {
            $this->_example_output($output);
        } else {
            return $output;
        }
    }

}