
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_daftar extends CI_Controller {

    public function index() {
        $this->load->view('form_daftar');}

        function create_member() {
            $this->load->library('form_validation');

            // field name, error message, validation rules

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');


            if ($this->form_validation->run() == FALSE) {
                $this->load->view('daftar_error');
            } else {
                $this->load->model('m_member');

                if ($this->m_member->insert_member()) {
                    //$data['main_content'] = 'signup_successful';
                     echo "<script type='text/javascript'>alert('Registrasi Berhasil. Silakan login menggunakan username dan password Anda');</script>";
                    $this->load->view('home_user');
                } else {
                    $this->load->view('daftar_error');
                }
            }
        }

    }



