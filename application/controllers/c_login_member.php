

<?php

//

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_login_member extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        session_start();
    }

//    public function index() {
//        $this->load->view("home_user");
//    }

    function cekuser() {
        $this->load->model('m_member');
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['hasil'] = $this->m_member->cek_member();

        if ($data['hasil'] == null) {
            return "no";
        } else {
            return "yes";
        }
    }

    function usermasuk() {
        if ($this->cekuser() == "yes") {
            //  $this->load->model('m_member');

            $data['username'] = $this->input->post('username');

            $newdata = array
                (
                'username' => $data['username'],
                'logged_in' => true
            );

//            $tanggal = '20130910';
//            $jml_pengunjung = 101;
//            
//            $data = array(
//                'jml_pengunjung' => $jml_pengunjung,
//                'date' => $tanggal
//            );
//
//            $this->db->update('member_login', $data, "date =" . $tanggal);

//            if  ($this->sess_expiration == 7200)
//{
//     $this->sess_expiration = (60);
//}
//             
            
            $this->session->set_userdata($newdata);
            $tanggal = date("d-m-Y");
            $this->load->model('m_user_online');
            $this->m_user_online->get_member_online($tanggal);

            $data['username'] = $newdata['username'];

            $this->load->view("home_member", $data);
        } else {
            echo "<script type='text/javascript'>alert('Login Gagal. Pastikan field username dan password terisi dan benar');</script>";
            //$this->load->controller('user/c_navigasi_user');
            $this->load->view("home_user");
        }
    }

    function logout() {
        $this->session->sess_destroy();
        $this->load->view("home_user");
    }

}
?>