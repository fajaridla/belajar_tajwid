<?php

class C_member extends CI_Controller {

    function index($id) {
        if ($this->session->userdata('logged_in') == TRUE) {
            switch ($id) {
                case 'home_member':
                    $this->load->view("welcome");
                    break;

                case 'materi_member':
                    $this->load->model('m_materi');
                    $id_tajwid = 'T01';
                    $data['hasil'] = $this->m_materi->get_materi($id_tajwid);
                    $this->load->view('content/makharijal', $data);
                    break;

                case 'komentar_member':
                    $this->load->view("form_komentar_member");
                    break;


                case 'info_member':
                    $this->load->model('m_info');
                    $data['hasil'] = $this->m_info->get_info();
                    $this->load->view("form_info", $data);
                    break;
            }
        } 
else {
           $this->session->sess_destroy();
        $this->load->view('home_user.php');
        }
    }

}

?>
