<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_jawaban extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_soal');
           session_start();
    }

    public function index() {
        if ($this->session->userdata('logged_in') == TRUE) {
            $data['nilai'] = $this->cek_nilai();
            $data['username']= $this->session->userdata('username');
            $this->load->view("nilai", $data);
        } 
  else {
           $this->session->sess_destroy();
        $this->load->view('home_user.php');
        }
    }

    public function cek_nilai() {

        if (isset($_POST['submit'])) {
            $pilihan = $this->input->post('pilihan');
            $id_soal = $this->input->post('id');

            $benar = 0;
            $salah = 0;
            $kosong = 0;


            for ($i = 0; $i < count($id_soal); $i++) {
                $id = $id_soal[$i];

                if (!empty($pilihan[$id])) {
                    $check = $this->m_soal->check_jawaban($id, $pilihan[$id]);
                    if ($check != NULL) {
                        $benar++;
                    } else {
                        $salah++;
                    }
                } else {

                    $kosong++;
                }
            }
            $score = $benar * 20;
        }
//        return $kosong;
        return $score;
//        return $salah;
    }

}
?>




