
<?php

//

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_login_admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->helper('url');
        session_start();
    }

    public function index() {
        $this->load->view("admin/form_admin");
    }

    function _example_output($output = null) {
        $this->load->view('admin/home_admin.php', $output);
    }

    function cekuser() {
        $this->load->model('m_admin');
        $data['name_admin'] = $this->input->post('username');
        $data['pass_admin'] = $this->input->post('password');
        $data['hasil'] = $this->m_admin->cek_admin();

        if ($data['hasil'] == null) {
            return "no";
        } else {
            return "yes";
        }
    }

    function usermasuk() {
        if ($this->cekuser() == "yes") {
           // $this->load->model('m_admin');

//            $data['username'] = $this->input->post('username');
//            $newdata = array('username' => $data['username'],
//                'logged_in' => TRUE);
//            $this->session->set_userdata($newdata);

            $data['name_admin'] = $this->input->post('username');

            $newdata = array
                (
                'name_admin' => $data['name_admin'],
                'is_logged_in' => true
            );

            $this->session->set_userdata($newdata);
            $data['name_admin'] = $newdata['name_admin'];
            //$dd = 'admin/header.php';
            
            $this->load->view('admin/header.php',$data);
            $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
        } else {
            echo "<script type='text/javascript'>alert('Login Gagal. Pastikan field username dan password terisi dan benar');</script>";
            //$this->load->controller('user/c_navigasi_user');
            $this->load->view("admin/form_admin.php");
        }
    }

    function logout() {
        $this->session->sess_destroy();
        $this->load->view("admin/form_admin.php");
    }

}

?>