<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_komentar_member extends CI_Controller {

        function __construct() {
        parent::__construct();
        $this->load->helper('url');
        session_start();
       // $this->username = $this->session->userdata('username');
    }
    function index(){}
//    public function index($side) {
//        if ($this->session->userdata('logged_in') == TRUE) {
//            switch ($side) {
//                case 'komentar':
////                $this->load->model('dbfunction');
//                    //$data['hasil'] = $this->dbfunction->get_hijaiyah();
//                    $this->load->view("form_komentar_member");
//                    break;
//            }
//        }
//    }

    function create_komentar() {
        if ($this->session->userdata('logged_in') == TRUE) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('komentar', 'comment', 'trim|required');

            if ($this->form_validation->run() == false) {
                $data['username']= $this->session->userdata('username');
                $this->load->view('komentar_error_member',$data);
            } else {

                $this->load->model('m_komentar');

                if ($this->m_komentar->insert_komentar()) {
                    echo "<script type='text/javascript'>alert('Terimakasih!! Masukan Anda telah diinput');</script>";
                    $data['username']= $this->session->userdata('username');
                    $this->load->view('home_member',$data);
                } else {
                    // $this->username = $this->session->userdata('username');
                    // $data['username']= $this->session->userdata('username');
                     $this->load->view('komentar_error_member');
//                    echo "<script type='text/javascript'>alert('maaf ada kesalahan');</script>";
                }
            }
        } 
      else {
           $this->session->sess_destroy();
        $this->load->view('home_user.php');
        }
    }

}
