<h3 style="padding:0px 30px 0px 30px; color:#7AB33E;">
    Assalamualaikum Wr.Wb.

</h3 >  
<p style="padding:0px 30px 0px 30px">

    Selamat datang di website <i>belajar-tajwid.com</i>. Website ini adalah sarana pembelajaran tajwid 
    gratis yang diperuntukan bagi pengunjung yang ingin mempelajari  bagaimana membaca huruf-huruf al-quran dengan baik dan benar sesuai dengan tuntunan nabi Muhammad Saw.<br><br>
    Tajwid adalah cabang ilmu yang mempelajari tatacara pembacaan huruf-huruf alquran dengan baik dan benar. Secara bahasa tajwid bermakna membaguskan, adapun secara istilah tajwid adalah tatacara mengeluarkan setiap huruf dari tempat  keluarnya dengan memberikan hak dan mustahaknya.   
</p>
<br>
<p>
<h3 style="padding:0px 30px 0px 30px;color:#7AB33E;">Tentang Kami
</h3 > 
<p style="padding:0px 30px 0px 30px">
    Website ini dibangun dalam rangka memudahkan para pengunjung untuk 
    dapat belajar tajwid secara mandiri kapan dan dimanapun.  
    Beberapa fitur yang terdapat didalam nya yaitu:<p style="padding:0px 30px 0px 30px;color:#7AB33E;">(1) Penjelasan hukum-hukum tajwid yang mudah untuk difahami. <br>
    (2) Contoh penerapan hukum tajwid dan pengucapan nya dalam bentuk audio. <br> (3) Aplikasi hukum-hukum tajwid dalam surat-surat pendek juz amma</p> 


<p style="padding:0px 30px 0px 30px">
    Sedangkan materi dan contoh yang disajikan dalam website ini mengacu pada:</p>
<p style="padding:0px 30px 0px 30px;color:#7AB33E;">(1) Kajian Ilmu Tajwid  yang dikarang oleh Abdul Aziz Abdur Ra'uf Al Hafidz, Lc.. <br>
    (2) Pelajaran Tajwid Praktis dan Ringkas yang ditulis oleh Iwan Purwanto. </p> 


<p style="padding:0px 30px 200px 30px">
    Semoga karya yang sederhana ini dapat bermanfaat dan digunakan sesuai dengan fungsinya.
    Salam Kami <i>Hamba-hamba Allah </i></p>

<br><br>