
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php $this->load->helper('url'); ?>
<html lang="en">

    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jQuery/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jQuery/css/PageStyle.css">

        <script type="text/javascript" src="<?php echo base_url(); ?>jQuery/js/jquery-1.9.1.js" charset="utf-8"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jQuery/js/jquery-ui-1.10.1.custom.js" charset="utf-8"></script>
        <title> Belajar Tajwid </title>

        <script>
                 
            $(function() {
                $( "#accordion" ).accordion();
            });

            //            $(function() {
            //                $( "input[type=submit],button" )
            //                .button()
            //                .click(function( event ) {
            //                    event.preventDefault();
            //                });
            //            });
            
            function toggleByClass(className) {
                $("."+className).toggle();
            }
            
            
            //            $.ajax({
            //                type: 'POST',
            //                url: 'c_pengantar/index/makharijal',
            //                data: {},
            //                success: function(data) {
            //                    $("#main_content").html(data);
            //                }
            //            })
            
      
            //            $(function() {
            //                $( "input[type=submit],button" )
            //                .button()
            //                .click(function( event ) {
            //                    event.preventDefault();
            //                });
            //            });                   
            
        </script>
        <script type="text/javascript">
            //alert("asdf");
            $(document).ready(function(){
                $('#daftar').click(function(){
            
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url(); ?>c_daftar',
                        data: {},
                        success: function(data) {
                            $(".main_content").html(data);
                        }
                    })
                })
            })
        </script>

    </head>

    <BODY>
        <div id="container" style="width:1024px; height: fit-content; margin:0 auto">
            <div id="header" style="width: 1024px; height:290px ">
                <?php include 'header_user.php'; ?>

            </div>

            <div  style="width: 1024px; padding-top: 5px" >
                <div class="menu_content" style="height: 1070px">
                    <?php include 'login.php'; ?>   


                </div>

                <div class="main_content" style="height:1070px; line-height:150%; ">
                    <div class="teori" style="padding-left: 30px;">
                        <center>
                            <h3 style="padding:0px 30px 0px 15px; color:#7AB33E;">
                                Registrasi Member
                            </h3 >
                            <hr>


                            <p style="padding:0px 30px 0px 30px">
                                Silakan melakukan registrasi member, setelah itu login menggunakan username dan password member untuk dapat memilih materi pembelajaran

                            </p>
                        </center>     
                    </div>  

                    <center>
                        <div class="panel_daftar" style="font-size:0.9em; "  >

                            <?php echo form_open('c_daftar/create_member'); ?>
                            <center>
                                <table style="padding-top:30px; ">


                                    <tr style="line-height: 30px;">
                                        <td> Username: </td>
                                        <td> <?php echo form_input('username', set_value('username')); ?> </td>
                                    </tr>
                                    <tr style="line-height: 30px;">
                                        <td > Password: </td>
                                        <td>  <?php echo form_password('password', set_value('password')); ?></td>
                                    </tr>

                                    <tr style="line-height: 30px;" >  
                                        <td> Email: </td>
                                        <td> <?php echo form_input('email', set_value('email')); ?> </td>
                                    </tr>
                                    <tr style="line-height: 30px;" >  
                                        <td> Alamat: </td>
                                        <td>  <?php echo form_input('alamat', set_value('alamat')); ?>  </td>
                                    </tr>

                                    <tr  style="line-height: 30px;">
                                        <td colspan="2" style="padding-left: 170px;"><button>submit</button></td>
                                        <td> </td>
                                    </tr>

                                </table>  
                                <?php echo validation_errors('<p class="error">'); ?>
                            </center>

                            <br>

                        </div>



                    </center>
                </div>
            </div>

            <div id="footer" style="width: 1024px; height:90px;">
                <?php include 'footer.php'; ?>
            </div>
        </div>

    </BODY>
</html>


