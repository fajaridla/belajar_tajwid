  <div class="teori">
      <p> Idghom dibagi kedalam dua kategori yaitu idghom bigunnah dan idghom bila gunnah.  Idghom bigunnah dibaca dengan dengung 
          sedangkan idghom bilagunnah dibaca dengan tidak mendengung. </p>
    </div> 


<table cellspacing="0" style="width:650px;">
     <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <tbody>
<tr> 
    <th class="tableHeader" scope="col">Cara Pengucapan</th>
    <th class="tableHeader" scope="col"> Jenis Idghom </th>
    <th class="tableHeader" scope="col">Huruf Hijaiyah</th> 
    <th class="tableHeader" scope="col"> Tanda</th> 
</tr>
<tr class="box">
    <td class="tableContent">Diucapkan dengan dengung </td>
    <td class="tableContent"> Bigunnah </td>
     <td class="tableContent">  <strong><font size="5+"> ي - ن - م - و </strong></font>   </td>
     <td class="tableContent"> <img src="images/tanda.jpg"> </td>  
</tr>

<tr class="box">
    <td class="tableContent">Diucapkan tanpa dengung </td>
    <td class="tableContent"> Bilagunnah </td>
     <td class="tableContent">  <strong><font size="5+"> ل - ر </strong></font>   </td>
     <td class="tableContent"> <img src="images/tanda.jpg"> </td>  
</tr>

</tbody>
</table>