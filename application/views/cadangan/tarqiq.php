
<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori" style="padding-bottom: 600px;">
        <?php include "overview_tarqiq.php"; ?>

        <center>

            <table cellspacing="0" style="width:650px; padding-left: 10px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Keadaan</th>
                        <th class="tableHeader" scope="col">Huruf</th> 
                        <th class="tableHeader" scope="col"> Contoh</th> 
                        <th class="tableHeader" scope="col"> Play </th> 
                    </tr>
                    <?php
                    $no_tarqiq = 1;
                    foreach ($hasil as $data):
                        ?>
                        <tr class="box">
                            <td class="tableContent"> <?php echo $data->keadaan_tarqiq; ?> </td>
                            <td class="tableContent"> <font size = "6+"><strong><?php echo $data->huruf_tarqiq; ?></strong> </font> </td>
                            <td class="tableContent" style="width: 200px;"> <font size="6+"><strong> <?php echo $data->contoh_tarqiq; ?>  </strong></font>  </td>

                            <td class="tableContent">
                                <audio id=<?php echo $data->no_tarqiq; ?>>
                                    <source src= <?php echo $data->suara_tarqiq; ?>>
                                </audio>
                                <button onclick="document.getElementById(<?php echo $data->no_tarqiq; ?>).play()"> Play </button>  


                            </td>   
                        </tr>
                        <?php
                        $no_tarqiq++;
                    endforeach;
                    ?>

                </tbody>
            </table>

        </center>

    </div> 

</center>