
<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori" style="padding-bottom: 600px;">
        <?php include "overview_tafkhim.php"; ?>

        <center>

            <table cellspacing="0" style="width:650px; padding-left: 10px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Keadaan</th>
                        <th class="tableHeader" scope="col">Huruf</th> 
                        <th class="tableHeader" scope="col"> Contoh</th> 
                        <th class="tableHeader" scope="col"> Play </th> 
                    </tr>
                    <?php
                    $no_tafkhim = 1;
                    foreach ($hasil as $data):
                        ?>
                        <tr class="box">
                            <td class="tableContent"> <?php echo $data->keadaan_tafkhim; ?> </td>
                            <td class="tableContent"> <font size = "6+"><strong><?php echo $data->huruf_tafkhim; ?></strong> </font> </td>
                            <td class="tableContent"> <font size="6+"><strong> <?php echo $data->contoh_tafkhim; ?>  </strong></font>  </td>

                            <td class="tableContent">
                                <audio id=<?php echo $data->no_tafkhim; ?>>
                                    <source src= <?php echo $data->suara_tafkhim; ?>>
                                </audio>
                                <button onclick="document.getElementById(<?php echo $data->no_tafkhim; ?>).play()"> Play </button>  


                            </td>   
                        </tr>
                        <?php
                        $no_tafkhim++;
                    endforeach;
                    ?>

                </tbody>
            </table>

        </center>

    </div> 

</center>