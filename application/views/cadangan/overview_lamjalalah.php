<h3>
    Hukum Taglizh dan Tarqiq
</h3> 
<p>
    Lam jalalah adalah huruf lam yang terdapat pada lafazh <b> الله</b> . Cara membacanya dibedakan menjadi dua: <b>Taglizh </b> dan 
    <b>Tarqiq</b>.
    Taglizh artinya tebal, lafazh <b>  الله </b> dibaca tebal jika didahului oleh huruf yang berharokat <i>fathah</i>  dan <i>dhommah</i>.
    Tarqiq artinya tipis, lafazh <b> الله </b>dibaca tipis jika didahului oleh huruf 
    yang berharokat <i>kasroh</i>.
</p>