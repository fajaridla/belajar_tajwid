
<h3>
    Hukum Mad Tobi'iy 
</h3> 
<p> Arti Mad Menurut Bahasa tambahan, sedangkan secara istilah memanjangkan lama  suara ketika 
    mengucapkan huruf mad. Mad Tobi'iy dibaca panjang 2 ketukan.</p>
<table cellspacing="0" style="width:650px; padding-bottom: 20px;">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <tbody>
        <tr> 
            <th class="tableHeader" scope="col">Huruf Mad</th>
            <th class="tableHeader" scope="col">Harokat Sebelumnya</th> 
            <th class="tableHeader" scope="col"> Cara Membaca</th> 
        </tr>
        <tr class="box">
            <td class="tableContent"><font size = "6+"> <strong>وْ </strong></font> </td>
            <td class="tableContent"> <font size = "6+">ُ </font> <strong>  </td>
            <td class="tableContent"> Memanjangkan lama suara 2 ketukan </td>

        </tr>
        <tr class="box">
            <td class="tableContent"><font size = "6+"> <strong>يْ </strong></font> </td>
            <td class="tableContent"> <font size = "6+">ِ</font> <strong>  </td>
            <td class="tableContent"> Memanjangkan lama suara 2 ketukan </td>

        </tr>
        <tr class="box">
            <td class="tableContent"><font size = "6+"> ا <strong></strong></font> </td>
            <td class="tableContent"> <font size = "6+">َ</font> <strong>  </td>
            <td class="tableContent"> Memanjangkan lama suara 2 ketukan </td>

        </tr>

    </tbody>
</table>

