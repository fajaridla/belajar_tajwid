<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori">
        <h3>
            Makharijal Huruf (Tempat Keluarnya Huruf)
        </h3> 
        <p>
            Secara Bahasa makhroj artinya tempat keluar. 
            Sedangkan menurut istilah makhroj adalah suatu nama tempat, yang padanya huruf dibentuk atau diucapkan 

        </p>
    </div> 

    <div style="height:40px; width:650px ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling_hijaiyah')" >Latin</button>
            <button onclick="toggleByClass('hijaiyah')" >Arab</button>
        </div>

    </div>

    <center>
        <div id="outer">
            <?php
            $ID_MATERI = 1;
            foreach ($hasil as $data) {
                ?>

                <div class="panel_huruf" > 
                    <center>
                        <div class="hijaiyah"><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                        <div class="spelling_hijaiyah">   <?php echo $data->LATIN; ?> </div>
                        <div class="play"> 
                            <audio id=<?php echo $data->ID_MATERI; ?>>
                                <source src= <?php echo $data->SUARA; ?>>
                            </audio>
                            <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                        </div>
                    </center>
                </div>


                <?php
                $ID_MATERI++;
            }
            ?>
        </div>

        <div style="height: 110px; 
             width: 100px;">

    </center>
    <div class="materi_toggle"  align="center" style=" width:650px;">

        <div  align="center" style="width:650px; height:40px;  float: left; padding-top:20px;">
            <button  onclick="toggleByClass('tabel_materi')" >Penjelasan</button>
        </div>
        <div class="tabel_materi" align="center" style="width:650px; padding-top:10px; padding-bottom:10px; display: none;">

            <table cellspacing="0" style="width:650px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Cara Pengucapan</th>
                        <th class="tableHeader" scope="col">Tempat Keluarnya</th> 
                        <th class="tableHeader" scope="col">Huruf Hijaiyah</th> 
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Suara keluar dari rongga mulut  menekan pada udara </td>
                        <td class="tableContent">Rongga Mulut </td>
                        <td class="tableContent">  <strong><font size="5+">ا - و - ي</strong></font>   </td>

                    </tr>
                    <tr class="box">
                        <td class="tableContent">Suara keluar dari tenggorokan bagain dalam</td>
                        <td class="tableContent">Rongga Mulut  </td>
                        <td class="tableContent">  <strong><font size="5+">ه - ء </strong></font>   </td>
                    </tr>

                    <tr class="box">
                        <td class="tableContent">Suara keluar dari tenggorokan bagain tengah</td>
                        <td class="tableContent">Tenggorokan   </td>
                        <td class="tableContent">  <strong><font size="5+">ح - ع </strong></font>   </td>
                    </tr>

                    <tr class="box">
                        <td class="tableContent">Suara keluar dari tenggorokan bagain luar</td>
                        <td class="tableContent">Tenggorokan   </td>
                        <td class="tableContent">  <strong><font size="5+">خ - غ </strong></font>   </td>   	
                    </tr>

                    <tr class="box">
                        <td class="tableContent"> Suara keluar dari bibir bawah bagian tengah dengan dengan ujung gigi atas</td>
                        <td class="tableContent"> Dua Bibir  </td>
                        <td class="tableContent">  <strong><font size="5+">ف </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Suara keluar dari paduan bibir atas dan bibir bawah  </td>
                        <td class="tableContent"> Dua Bibir </td>
                        <td class="tableContent">  <strong><font size="5+">و - م - ب </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Suara keluar dari pangkal lidah dan langit-langit atas</td>
                        <td class="tableContent">  Lidah </td>
                        <td class="tableContent">  <strong><font size="5+">  ق </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Pangkal lidah, kedepan sedikit dari makhraj qof, dengan langit-langit atas </td>
                        <td class="tableContent">  Lidah </td>
                        <td class="tableContent">  <strong><font size="5+">  ك </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Pertengahan lidah dimantapkan dengan langit-langit atas </td>
                        <td class="tableContent"> Lidah   </td>
                        <td class="tableContent">  <strong><font size="5+"> ش - ج - ي </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Tepi lidah dengan graham kiri atau kanan </td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ض </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan langit-langit di hadapannya </td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ل </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Bergeser kebawah sedikit dari makhraj lam, dengan langit-langit dihadapannya</td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ن </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Dekat makhraj nun, tapi masuk pada punggung lidah </td>
                        <td class="tableContent">   Lidah </td>
                        <td class="tableContent">  <strong><font size="5+">ر </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan pangkal gigi seri atas </td>
                        <td class="tableContent">  Lidah </td>
                        <td class="tableContent">  <strong><font size="5+"> ت - د - ط </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan ujung gigi seri atas </td>
                        <td class="tableContent"> Lidah   </td>
                        <td class="tableContent">  <strong><font size="5+"> ز - ظ - ث </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan ujung gigi seri bawah  </td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ص - ز - س </strong></font>   </td>   	
                    </tr>


                </tbody>
            </table>
        </div>

    </div>

</center>

