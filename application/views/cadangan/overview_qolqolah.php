
<h3>
    Hukum Qolqolah (bergerak dan gemetar)
</h3> 
<p>
    Qolqolah Suara pantulan yang jelas dan terjadi pada huruf yang bersukun. 
    Cara membecanya dengan menekan makhraj huruf tersebut. Qolqolah dibagi kedalam dua kelompok besar. Kelompok pertama
    jika huruf bersukun terdapat di tengah kalimat (qoloqolah <i>shugro</i>). Sedangkan qolqolah <i>kubro</i> jika huruf qolqolah bersukun karena diwakofkan di akhir kalimat.
</p>

<table cellspacing="0" style="width:650px;">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <tbody>
        <tr> 
            <th class="tableHeader" scope="col">Huruf Qolqolah</th>
            <th class="tableHeader" scope="col">Tanda</th> 
            <th class="tableHeader" scope="col"> Contoh</th> 
            <th class="tableHeader" scope="col"> Dibaca </th> 
            <th class="tableHeader" scope="col"> Bunyi </th> 
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ب </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> اَبْ </strong></font>  </td>
            <td class="tableContent">  ab(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ج </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> يَجْ </strong></font>  </td>
            <td class="tableContent">  yaj(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> د </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> اَدْ</strong></font>  </td>
            <td class="tableContent">  ad(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ط</strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong>مَطْ </strong></font>  </td>
            <td class="tableContent">  math(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ق </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> تَقْ </strong></font>  </td>
            <td class="tableContent">  taq(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>

    </tbody>
</table>