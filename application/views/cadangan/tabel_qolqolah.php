
<table cellspacing="0" style="width:650px;">
     <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <tbody>
<tr> 
    <th class="tableHeader" scope="col">Huruf Qolqolah</th>
    <th class="tableHeader" scope="col">Tanda</th> 
    <th class="tableHeader" scope="col"> Contoh</th> 
    <th class="tableHeader" scope="col"> Dibaca </th> 
    <th class="tableHeader" scope="col"> Bunyi </th> 
</tr>
<tr class="box">
     <td class="tableContent"><font size="6+"><strong> ب </strong></font>  </td>
     <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
     <td class="tableContent"> <font size="6+"><strong> اَبْ </strong></font>  </td>
     <td class="tableContent">  ab(e) </td>
     <td class="tableContent">  <button> Play </button> </td>   
</tr>
<tr class="box">
     <td class="tableContent"><font size="6+"><strong> ج </strong></font>  </td>
     <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
     <td class="tableContent"> <font size="6+"><strong> يَجْ </strong></font>  </td>
     <td class="tableContent">  yaj(e) </td>
     <td class="tableContent">  <button> Play </button> </td>   
</tr>
<tr class="box">
     <td class="tableContent"><font size="6+"><strong> د </strong></font>  </td>
     <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
     <td class="tableContent"> <font size="6+"><strong> اَدْ</strong></font>  </td>
     <td class="tableContent">  ad(e) </td>
     <td class="tableContent">  <button> Play </button> </td>   
</tr>
<tr class="box">
     <td class="tableContent"><font size="6+"><strong> ط</strong></font>  </td>
     <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
     <td class="tableContent"> <font size="6+"><strong>مَطْ </strong></font>  </td>
     <td class="tableContent">  math(e) </td>
     <td class="tableContent">  <button> Play </button> </td>   
</tr>
<tr class="box">
     <td class="tableContent"><font size="6+"><strong> ق </strong></font>  </td>
     <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
     <td class="tableContent"> <font size="6+"><strong> تَقْ </strong></font>  </td>
     <td class="tableContent">  taq(e) </td>
     <td class="tableContent">  <button> Play </button> </td>   
</tr>

</tbody>
</table>