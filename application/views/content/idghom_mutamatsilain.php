<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>

<center>
    <div class="teori">
        <h3>
            Melebur Dua Huruf yang Sama (Idghom Mutamatsilain)
        </h3> <hr>
        <p>
            Idghom Mutamatsilain terjadi apabila berhadapan 2 huruf yang sama cara pengucapan dan sifatnya. 
            Cara Membacanya, dua huruf yang sama tersebut dilebur menjadi satu.
        </p>
    </div> 

    <div style="height:40px; width:650px ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyahPanjang')" >Arab</button>
        </div>

    </div>



     <center>
        <div id="outerSambung" style="padding-left:30px;">
            <?php
            $ID_MATERI = 1;
            foreach ($hasil as $data) {
                ?>

                <div class="panel_hurufSambungPanjang" > 
                    <center>
                        <div class="hijaiyahPanjang" ><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                        <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                        <div class="play"> 
                            <audio id=<?php echo $data->ID_MATERI; ?>>
                                  <source src= <?php echo base_url()."assets/sound_materi/".$data->SUARA; ?>>
                            </audio>
                            <button onclick="document.getElementById(<?php echo  $data->ID_MATERI; ?>).play()"> Play </button>  
                        </div>
                    </center>
                </div>


                <?php
                $ID_MATERI++;
            }
            ?>
        </div>
    </center>


<!--    <div class="materi_toggle"  align="center" style=" width:650px;">

        <div id="button" align="center" style="width:600px; height:40px;  float: left; padding-top:20px;">
            <button  onclick="toggleByClass('tabel_materi')" >Penjelasan</button>
        </div>
        <div class="tabel_materi" align="center" style="width:650px; padding-top:10px; padding-bottom:100px; display: none;">
            <div class="teori">

            </div> 


            <table cellspacing="0" style="width:650px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Cara Pengucapan</th>
                        <th class="tableHeader" scope="col">Huruf Hijaiyah</th> 
                        <th class="tableHeader" scope="col"> Tanda</th> 
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Mim dibaca dengung </td>
                        <td class="tableContent">  <strong><font size="5+">م</strong></font>   </td>
                        <td class="tableContent"> <strong><font size="5+"> مْ </strong></font>   </td>


                    </tr>

                </tbody>
            </table>

        </div>

    </div>-->

</center>
