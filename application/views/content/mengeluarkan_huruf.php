<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>


<center>
    <div class="teori">
        <div class="teori">
            <h3>
                Cara Mengeluarkan Huruf Hijaiyah
            </h3> <hr>

        </div>
    </div> 



    <center>
        <div id="outerSambung" style="padding-left:10px; padding-top: 20px;">
            <table cellspacing="0" style="width:650px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Cara Pengucapan</th>
                        <th class="tableHeader" scope="col">Tempat Keluarnya</th> 
                        <th class="tableHeader" scope="col">Huruf Hijaiyah</th> 
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Suara keluar dari rongga mulut  menekan pada udara </td>
                        <td class="tableContent">Rongga Mulut </td>
                        <td class="tableContent">  <strong><font size="5+">ا - و - ي</strong></font>   </td>

                    </tr>
                    <tr class="box">
                        <td class="tableContent">Suara keluar dari tenggorokan bagain dalam</td>
                        <td class="tableContent">Rongga Mulut  </td>
                        <td class="tableContent">  <strong><font size="5+">ه - ء </strong></font>   </td>
                    </tr>

                    <tr class="box">
                        <td class="tableContent">Suara keluar dari tenggorokan bagain tengah</td>
                        <td class="tableContent">Tenggorokan   </td>
                        <td class="tableContent">  <strong><font size="5+">ح - ع </strong></font>   </td>
                    </tr>

                    <tr class="box">
                        <td class="tableContent">Suara keluar dari tenggorokan bagain luar</td>
                        <td class="tableContent">Tenggorokan   </td>
                        <td class="tableContent">  <strong><font size="5+">خ - غ </strong></font>   </td>   	
                    </tr>

                    <tr class="box">
                        <td class="tableContent"> Suara keluar dari bibir bawah bagian tengah dengan dengan ujung gigi atas</td>
                        <td class="tableContent"> Dua Bibir  </td>
                        <td class="tableContent">  <strong><font size="5+">ف </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Suara keluar dari paduan bibir atas dan bibir bawah  </td>
                        <td class="tableContent"> Dua Bibir </td>
                        <td class="tableContent">  <strong><font size="5+">و - م - ب </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Suara keluar dari pangkal lidah dan langit-langit atas</td>
                        <td class="tableContent">  Lidah </td>
                        <td class="tableContent">  <strong><font size="5+">  ق </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Pangkal lidah, kedepan sedikit dari makhraj qof, dengan langit-langit atas </td>
                        <td class="tableContent">  Lidah </td>
                        <td class="tableContent">  <strong><font size="5+">  ك </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Pertengahan lidah dimantapkan dengan langit-langit atas </td>
                        <td class="tableContent"> Lidah   </td>
                        <td class="tableContent">  <strong><font size="5+"> ش - ج - ي </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Tepi lidah dengan graham kiri atau kanan </td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ض </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan langit-langit di hadapannya </td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ل </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Bergeser kebawah sedikit dari makhraj lam, dengan langit-langit dihadapannya</td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ن </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Dekat makhraj nun, tapi masuk pada punggung lidah </td>
                        <td class="tableContent">   Lidah </td>
                        <td class="tableContent">  <strong><font size="5+">ر </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan pangkal gigi seri atas </td>
                        <td class="tableContent">  Lidah </td>
                        <td class="tableContent">  <strong><font size="5+"> ت - د - ط </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan ujung gigi seri atas </td>
                        <td class="tableContent"> Lidah   </td>
                        <td class="tableContent">  <strong><font size="5+"> ز - ظ - ث </strong></font>   </td>   	
                    </tr>
                    <tr class="box">
                        <td class="tableContent"> Ujung lidah dengan ujung gigi seri bawah  </td>
                        <td class="tableContent">  Lidah  </td>
                        <td class="tableContent">  <strong><font size="5+"> ص - ز - س </strong></font>   </td>   	
                    </tr>


                </tbody>
            </table>
        </div>
        </div>
    </center>




</center>











