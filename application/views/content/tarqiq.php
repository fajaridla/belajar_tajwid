<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>

<center>
    <div class="teori">
        <h3>
            Bunyi Huruf Ra' Menipis (tarqiq)
        </h3> <hr>
        <p>
            Tarqiq berarti menipiskan suara huruf. 
            Terdapat beberapa keadaan huruf ra dibaca dengan tipis /tarqiq.
        </p>
        <ul>
            <li>Ketika berharakat Kasroh</li>
            <li>Ra' Sukun  sebelumnya berharakat fathah</li>
            <li>Ra' Sukun karena waqof sebelumnya huruf berharakat kasroh atau ya sukun</li>
        </ul>
    </div> 





    <table cellspacing="0" style="width:650px;">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <tbody>
            <tr> 
                <th class="tableHeader" scope="col">Latin</th>
                <th class="tableHeader" scope="col"> Contoh Ayat </th> 
                <th class="tableHeader" scope="col"> Bunyi </th> 

            </tr>

            <?php
            $ID_MATERI = 0;
            foreach ($hasil as $data) {
                ?>
                <tr class="box">
                    <td class="tableContent"><?php echo $data->LATIN; ?></td>
                    <td class="tableContent"> <strong><font size = "6+"> <?php echo $data->HIJAIYAH; ?> </font> </strong>  </td>                  
                    <td class="tableContent">  <audio id=<?php echo $data->ID_MATERI; ?>>
                            <source src= <?php echo base_url() ."assets/sound_materi/". $data->SUARA; ?>>
                        </audio>
                        <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>   </td>   
                </tr>
                <?php
                $ID_MATERI++;
            }
            ?>

        </tbody>
    </table>
</center>