<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>

<center>
    <div class="teori">
        <h3>
            Berhenti Sejenak Tanpa Bernafas (Saktah)
        </h3> <hr>
        <p>
            Istilah Saktah merujuk pada pemberhentian sejenak bacaan Al-Qur'an ketika bertemu dengan tanda saktah (س). 
            Terdapat 4 Tempat saktah dalam Al-Qur'an.
        </p>
    </div> 





    <table cellspacing="0" style="width:650px;">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <tbody>
            <tr> 
                <th class="tableHeader" scope="col">Letak</th>
                <th class="tableHeader" scope="col"> Ayat </th> 
                <th class="tableHeader" scope="col"> Bunyi </th> 

            </tr>

            <?php
            $ID_MATERI = 0;
            foreach ($hasil as $data) {
                ?>
                <tr class="box">
                    <td class="tableContent"><?php echo $data->LATIN; ?></td>
                    <td class="tableContent"> <strong><font size = "6+"> <?php echo $data->HIJAIYAH; ?> </font> </strong>  </td>                  
                    <td class="tableContent">  <audio id=<?php echo $data->ID_MATERI; ?>>
                            <source src= <?php echo base_url() ."assets/sound_materi/". $data->SUARA; ?>>
                        </audio>
                        <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>   </td>   
                </tr>
                <?php
                $ID_MATERI++;
            }
            ?>

        </tbody>
    </table>
</center>