<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>


<center>
    <div class="teori">
        <h3>
            Bunyi Huruf Jelas (Idzhar)
        </h3> <hr>
        <p>
                        nun mati atau tanwin yang bertemu huruf idzhar dibaca jelas dan terang tanpa mendengung.
                        Idzhar secara bahasa berarti jelas atau terang. 
        </p>
    </div> 

    <div style="height:40px; width:650px ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyahPanjang')" >Arab</button>
        </div>

    </div>


    <center>
        <div id="outerSambung" style="padding-left:30px;">
            <?php
            $ID_MATERI = 1;
            foreach ($hasil as $data):
                ?>

                <div class="panel_hurufSambung" > 
                    <center>
                        <div class="hijaiyahPanjang"><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                        <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                        <div class="play"> 
                            <audio id=<?php echo $data->ID_MATERI; ?>>
                                <source src= <?php echo base_url()."assets/sound_materi/".$data->SUARA; ?>>
                            </audio>
                            <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                        </div>
                    </center>
                </div>


                <?php
                $ID_MATERI++;
            endforeach;
            ?>
        </div>
    </center>


    <div class="materi_toggle"  align="center" style=" width:650px; ">

        <div align="center" style="width:650px; height:40px;  float: left; padding-top:20px;">
            <button  onclick="toggleByClass('tabel_materi')" >Penjelasan</button>
        </div>
        <div class="tabel_materi" align="center" style="width:650px; padding-bottom:100px; display: none;">
            <div class="teori">
                <p> Hukum idzhar terjadi apabila nun bersukun atau tanwin 
                    bertemu dengan huruf-huruf yang keluar dari tenggorakan . Berikut adalah huruf-huruf tersebut: </p>
            </div> 


            <table cellspacing="0" style="width:650px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Cara Pengucapan</th>
                        <th class="tableHeader" scope="col">Huruf Hijaiyah</th> 
                        <th class="tableHeader" scope="col"> Tanda</th> 
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Diucapkan tanpa  dengung </td>
                        <td class="tableContent">  <strong><font size="5+"> ا - ه - ع - ح - غ - خ </strong></font>   </td>
                        <td class="tableContent">  <img src= <?php echo base_url()."images/tanda.jpg"; ?>> </td>
                           


                    </tr>

                </tbody>
            </table>
        </div>
    </div>


</center>