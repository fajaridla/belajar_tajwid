<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori">
        <h3>
           Menebal dan Menipis (Tafkhim dan Tarqiq)
        </h3> 
        <hr>
        <p>
           
           
            <b> Tafkhim </b> artinya tebal, lafazh <b>  الله </b> dibaca menebal jika didahului oleh huruf yang berharokat <i>fathah</i>  dan <i>dhommah</i>.
            <br><br><b>Tarqiq</b> artinya tipis, lafazh <b> الله </b>dibaca menepis jika didahului oleh huruf 
            yang berharokat <i>kasroh</i>.
        </p>
    </div> 

    <div style="height:40px; width:650px ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyah')" >Arab</button>
        </div>

    </div>


    <center>
        <div id="outerSambung" style="padding-left:30px;">
            <?php
            $ID_MATERI = 1;
            foreach ($hasil as $data):
                ?>

                <div class="panel_hurufSambung" > 
                    <center>
                        <div class="hijaiyah"><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                        <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                        <div class="play"> 
                            <audio id=<?php echo $data->ID_MATERI; ?>>
                                  <source src= <?php echo base_url()."assets/sound_materi/".$data->SUARA; ?>>
                            </audio>
                            <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                        </div>
                    </center>
                </div>


                <?php
                $ID_MATERI++;
            endforeach;
            ?>
        </div>
    </center>
</center>
