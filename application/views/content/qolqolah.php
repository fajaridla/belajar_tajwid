<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori">
        
<h3>
    Membaca Huruf Memantul (Qolqolah)
</h3> <hr>
<p>
    Suara pantulan terjadi pada huruf-huruf tertentu yang bersukun. 
    Cara membacanya dengan menekan   huruf tersebut. Qolqolah dibagi kedalam dua kelompok besar. <br><br><b>Qolqolah <i>sugro</i> </b>(bunyi pantul kecil)
    jika huruf bersukun terdapat di tengah kalimat (qoloqolah <i>shugro</i>). <br><br> <b>qolqolah <i>kubro</i> </b>(bunyi pantul besar) terjadi jika huruf qolqolah bersukun karena diwakofkan di akhir kalimat.
</p>

<!--<table cellspacing="0" style="width:650px;">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <tbody>
        <tr> 
            <th class="tableHeader" scope="col">Huruf Qolqolah</th>
            <th class="tableHeader" scope="col">Tanda</th> 
            <th class="tableHeader" scope="col"> Contoh</th> 
            <th class="tableHeader" scope="col"> Dibaca </th> 
            <th class="tableHeader" scope="col"> Bunyi </th> 
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ب </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> اَبْ </strong></font>  </td>
            <td class="tableContent">  ab(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ج </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> يَجْ </strong></font>  </td>
            <td class="tableContent">  yaj(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> د </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> اَدْ</strong></font>  </td>
            <td class="tableContent">  ad(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ط</strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong>مَطْ </strong></font>  </td>
            <td class="tableContent">  math(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>
        <tr class="box">
            <td class="tableContent"><font size="6+"><strong> ق </strong></font>  </td>
            <td class="tableContent"> <font size = "8+">ْ </font> <strong>  </td>
            <td class="tableContent"> <font size="6+"><strong> تَقْ </strong></font>  </td>
            <td class="tableContent">  taq(e) </td>
            <td class="tableContent">  <button> Play </button> </td>   
        </tr>

    </tbody>
</table>-->
    </div> 

    <div style="height:40px; width:650px; padding-top:20px; ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyah')" >Arab</button>
        </div>

    </div>

    <div  class="outer" >
        <center>
            <div id="outerSambung" style="padding-left:30px;">
                <?php
                $ID_MATERI = 1;
                foreach ($hasil as $data):
                    ?>

                    <div class="panel_hurufSambung" > 
                        <center>
                            <div class="hijaiyah"><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                            <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                            <div class="play"> 
                                <audio id=<?php echo $data->ID_MATERI; ?>>
                                     <source src= <?php echo base_url()."assets/sound_materi/".$data->SUARA; ?>>
                                </audio>
                                <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                            </div>
                        </center>
                    </div>


                    <?php
                    $ID_MATERI++;
                endforeach;
                ?>
            </div>
        </center>
    </div>


</center>