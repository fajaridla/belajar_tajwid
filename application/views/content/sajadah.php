<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>

<center>
    <div class="teori">
        <h3>
            Sujud Tilawah (Sajadah)
        </h3> <hr>
        <p>
            Istilah Sajadah merujuk pada sujud yang dilakukan ketika membaca ayat Al-Qur'an tertentu  yang bertanda sajadah.  Ayat Sajadah terdapat pada 15 surat Al-Quran : <br><br>(QS. 7:206),(QS. 13:15),(QS. 16:50),(QS. 17:709),(QS. 19:58),
            (QS. 22:18),(QS. 22:27),(QS. 25:60),(QS. 27:26),(QS. 32:15),(QS. 38:24),(QS. 41:37),(QS. 53:62),(QS. 84:21),(QS. 96:19).<br><br>
            Sujud ini sunnah dilakukan di dalam dan di luar solat, disunnahkan pula baik bagi yang membaca ataupun yang mendengarkan.
            Disyaratkan bagi yang melakukan menghadap kiblat, suci dari hadats, boleh dilakukan dengan diawali berdiri atau duduk, boleh juga diawali dengan
            takbiratul ihram atau tanpa dengannya, diakhiri tanpa salam atau dengan nya. doa yang dianjurkan :
        </p><br>
        <p><font size="5+"><strong>  "سَجَدَ وَجْهِيَ لِلَّذِى خَلَقَهُ وَصَوَّرَهُ وَشَقَّ سَمْعَهُ وَبَصَرَهُ تَبَارَكَ اللَّهُ أَحْسَنُ الْخَالِقِينَ" </strong></font></p>

        <center>
         (Sajada wajhiya lilladzii kholaqohuu wa syaqqo sam’ahuu wa bashorohuu bihaulihii wa quwwatihii fatabaarokallahu ahsanul khooliqiin)<br><br>

            Artinya : “Wajahku bersujud kepada (Allah) Yang telah menciptakannya dan membuka pendengaran dan penglihatannya dengan daya dan kekuatan-Nya. Maha suci Allah sebaik-baik Pencipta.” (Doa ini berdasarkan Hadits riwayat Ahmad, Abdu Daud dan Tirmidzi).
        </p></center>
        <p>
           
        </p>
    </div> 






</center>