<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori">

        <h3>
            Memanjangkan 2 Harkat (Mad Tobi'iy)
        </h3> <hr>
        <p> Arti Mad Menurut Bahasa tambahan, sedangkan secara istilah memanjangkan lama  suara ketika 
            mengucapkan huruf mad. Mad Tobi'iy dibaca panjang 2 ketukan.</p>
        <table cellspacing="0" style="width:650px; padding-bottom: 20px;">
            <meta http-equiv="content-type" content="text/html; charset=utf-8" />
            <tbody>
                <tr> 
                    <th class="tableHeader" scope="col">Huruf Mad</th>
                    <th class="tableHeader" scope="col">Harokat Sebelumnya</th> 
                    <th class="tableHeader" scope="col"> Cara Membaca</th> 
                </tr>
                <tr class="box">
                    <td class="tableContent"><font size = "6+"> <strong>وْ </strong></font> </td>
                    <td class="tableContent"> <font size = "6+">ُ </font> <strong>  </td>
                    <td class="tableContent"> Memanjangkan lama suara 2 ketukan </td>

                </tr>
                <tr class="box">
                    <td class="tableContent"><font size = "6+"> <strong>يْ </strong></font> </td>
                    <td class="tableContent"> <font size = "6+">ِ</font> <strong>  </td>
                    <td class="tableContent"> Memanjangkan lama suara 2 ketukan </td>

                </tr>
                <tr class="box">
                    <td class="tableContent"><font size = "6+"> ا <strong></strong></font> </td>
                    <td class="tableContent"> <font size = "6+">َ</font> <strong>  </td>
                    <td class="tableContent"> Memanjangkan lama suara 2 ketukan </td>

                </tr>

            </tbody>
        </table>

    </div> 

    <div style="height:40px; width:650px ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyah')" >Arab</button>
        </div>

    </div>


    <center>
        <div id="outerSambung" style="padding-left:30px;">
            <?php
            $ID_MATERI = 1;
            foreach ($hasil as $data):
                ?>

                <div class="panel_hurufSambung" > 
                    <center>
                        <div class="hijaiyah"><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                        <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                        <div class="play"> 
                            <audio id=<?php echo $data->ID_MATERI; ?>>
                                  <source src= <?php echo base_url()."assets/sound_materi/".$data->SUARA; ?>>
                            </audio>
                            <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                        </div>
                    </center>
                </div>


                <?php
                $ID_MATERI++;
            endforeach;
            ?>
        </div>
    </center>

    <!--    <div class="materi_toggle"  align="center" style=" width:650px; ">
    <?php include "penjelasan_idzhar.php"; ?>
        </div>-->

</center>