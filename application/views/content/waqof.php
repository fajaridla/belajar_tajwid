<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>
<center>
    <div class="teori">

        <h3>
            Mengentikan Bacaan (Waqof)
        </h3> <hr>
        <p>
           Waqof artinya berhenti di suatu kata ketika membaca Al-Quran, baik di akhir ayat maupun di tengah ayat. Berhenti 
           ketika membaca Al-Qur'an, memerlukan pengetahuan yang khusus, agar tilawah terdengar bagus dan tidak merusak makna dari ayat yang dibaca. 
           Berikut diantara tanda-tanda waqof dalam Al-Qur'an:
        </p>

        <table cellspacing="0" style="width:650px;">
            <meta http-equiv="content-type" content="text/html; charset=utf-8" />
            <tbody>
                <tr> 
                    <th class="tableHeader" scope="col">Tanda</th>
                    <th class="tableHeader" scope="col">Ketentuan</th> 

                </tr>
                <tr class="box">
                    <td class="tableContent"><img src=<?php echo base_url().'images/waqof/berhenti1.PNG' ?>> </td>
                    <td class="tableContent"> Harus Berhenti </td>

                </tr>
                <tr class="box">
                    <td class="tableContent"><img src=<?php echo base_url().'images/waqof/berhenti2.PNG' ?>> </td>
                    <td class="tableContent"> Boleh memilih Berhenti atau melanjutkan bacaan  </td>

                </tr>
                <tr class="box">
                    <td class="tableContent"><img src=<?php echo base_url().'images/waqof/berhenti3.PNG' ?>>  </td>
                    <td class="tableContent"> Dilarang berhenti, kecuali di akhir ayat	</td>

                </tr>


            </tbody>
        </table>
    </div> 

    <div style="height:40px; width:650px; padding-top:20px; ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyahPanjang')" >Arab</button>
        </div>

    </div>

    <div  class="outer" >
        <center>
            <div id="outerSambung" style="padding-left:30px;">
                <?php
                $ID_MATERI = 1;
                foreach ($hasil as $data):
                    ?>

                    <div class="panel_hurufSambungPanjang" > 
                        <center>
                            <div class="hijaiyahPanjang"> <img src=<?php echo base_url()."assets/img_hijaiyah/".$data->HIJAIYAH_IMG; ?>> </div>
                            <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                            <div class="play"> 
                                <audio id=<?php echo $data->ID_MATERI; ?>>
                                    <source src= <?php echo base_url() ."assets/sound_materi/". $data->SUARA; ?>>
                                </audio>
                                <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                            </div>
                        </center>
                    </div>


                    <?php
                    $ID_MATERI++;
                endforeach;
                ?>
            </div>
        </center>
    </div>


</center>