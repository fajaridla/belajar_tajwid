<script>
    $(function() {
        $( "input[type=submit],button" )
        .button()
        .click(function( event ) {
            event.preventDefault();
        });
    });                        
</script>


<center>
    <div class="teori">
        <div class="teori">
            <h3>
                Bunyi Huruf Melebur (Idghom)
            </h3> <hr>
            <p>
                Pengucapan nun mati atau tanwin harus melebur ketika bertemu huruf-huruf idghom, 
                atau pengucapannya dua huruf yang di tasydidkan.Idghom artinya memasukan
            </p>
        </div>
    </div> 

    <div style="height:40px; width:650px ">
        <div id="button" style="float: left; padding-bottom: 2px; ">
            <button  onclick="toggleByClass('spelling')" >Latin</button>
            <button onclick="toggleByClass('hijaiyahPanjang')" >Arab</button>
        </div>

    </div>

    <center>
        <div id="outerSambung" style="padding-left:30px;">
            <?php
            $ID_MATERI = 1;
            foreach ($hasil as $data):
                ?>

                <div class="panel_hurufSambung" > 
                    <center>
                        <div class="hijaiyahPanjang"><strong><font size="6+"><?php echo $data->HIJAIYAH; ?></strong></font></div>
                        <div class="spelling">   <?php echo $data->LATIN; ?> </div>
                        <div class="play"> 
                            <audio id=<?php echo $data->ID_MATERI; ?>>
                                <source src= <?php echo base_url()."assets/sound_materi/".$data->SUARA; ?>>
                            </audio>
                            <button onclick="document.getElementById(<?php echo $data->ID_MATERI; ?>).play()"> Play </button>  
                        </div>
                    </center>
                </div>


                <?php
                $ID_MATERI++;
            endforeach;
            ?>
        </div>
    </center>


    <div class="materi_toggle"  align="center" style=" width:650px;">

        <div align="center" style="width:650px; height:40px; padding-top:20px; float: left; ">
            <button  onclick="toggleByClass('tabel_materi')" >Penjelasan</button>
        </div>
        <div class="tabel_materi" align="center" style="width:650px; padding-top:10px; padding-bottom:100px; display: none;">
            <div class="teori">
                <p> Idghom dibagi kedalam dua kategori yaitu idghom bigunnah dan idghom bila gunnah.   </p>
            </div> 

            <table cellspacing="0" style="width:650px;">
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <tbody>
                    <tr> 
                        <th class="tableHeader" scope="col">Cara Pengucapan</th>
                        <th class="tableHeader" scope="col"> Jenis Idghom </th>
                        <th class="tableHeader" scope="col">Huruf Hijaiyah</th> 
                        <th class="tableHeader" scope="col"> Tanda</th> 
                    </tr>
                    <tr class="box">
                        <td class="tableContent">Diucapkan dengan dengung </td>
                        <td class="tableContent"> Bigunnah </td>
                        <td class="tableContent">  <strong><font size="5+"> ي - ن - م - و </strong></font>   </td>
                        <td class="tableContent"> <img src= <?php echo base_url()."images/tanda.jpg"; ?>> </td>  
                    </tr>

                    <tr class="box">
                        <td class="tableContent">Diucapkan tanpa dengung </td>
                        <td class="tableContent"> Bilagunnah </td>
                        <td class="tableContent">  <strong><font size="5+"> ل - ر </strong></font>   </td>
                        <td class="tableContent"> <img src= <?php echo base_url()."images/tanda.jpg"; ?>></td>  
                    </tr>

                </tbody>
            </table>
        </div>


    </div>

</center>
