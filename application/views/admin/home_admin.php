


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<title>
    Halaman Admin
</title>
<body>
    <div>
        <div id="header">
                      
        </div>
     
    </div>
	<div>
		<a href='<?php echo site_url('c_admin_manage/matters_management')?>'>Materi</a> |
		<a href='<?php echo site_url('c_admin_manage/question_management')?>'>Soal</a> |
		<a href='<?php echo site_url('c_admin_manage/info_management')?>'>Info</a> |
		<a href='<?php echo site_url('c_admin_manage/comment_management')?>'>Komentar</a> |		 
		<a href='<?php echo site_url('c_admin_manage/visitor_counter')?>'>Daftar Pengunjung </a> |		 
		
                <?php echo anchor('c_login_admin/logout', 'Logout'); ?>
                
                 </p>
                <hr>
		
	</div>
	<div style='height:20px;'> </div>  
    <div>
		
        <?php echo $output; ?>
        
    </div> 
         
        <div id="total">
                      
        </div>
     
    
</body>
</html>
