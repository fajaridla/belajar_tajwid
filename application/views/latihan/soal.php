<div style="padding:5px 5px 0px 30px; text-align:left; ">
    <div class="teori" style="font-size: 14px;">
        <h4>
            Latihan Soal 
        </h4> <hr><!--
        <p>
            Berikut tersedia beberapa soal sebagai bahan evaluasi pembelajaran tajwid yang sudah
            dilakukan sebelumnya. Selamat Menjawab.

        </p>-->


        <?php
        $no_soal = 0;
        //$jumlah = count($hasil);
        foreach ($hasil as $data):
            $id_soal = $data->ID_SOAL;
            $butir_soal = $data->BUTIR_SOAL;
            $jawaban_a = $data->JAWABAN_A;
            $jawaban_b = $data->JAWABAN_B;
            $jawaban_c = $data->JAWABAN_C;
            $jawaban_d = $data->JAWABAN_D;


            $sound_soal = $data->SOUND_SOAL;
            ?>



            <?php echo form_open('c_jawaban/index') ?>

            <input type="hidden" name="id[]" value=<?php echo $id_soal; ?>>
            <table >
                <tr>
                <tr>
                    <td><?php echo $no_soal = $no_soal + 1; ?>.</td>                                                 
                    <td><?php echo $butir_soal; ?>

                        <audio id=<?php echo $id_soal; ?>>
                            <source src= <?php echo base_url()."assets/files_soal/". $sound_soal;?> > 
                        </audio>               
                        <?php
                        $var = $sound_soal;
                        if ($var != NULL) {
                            ?> 
                            <img style="cursor: pointer;" src= <?php echo base_url() . 'jQuery/css/images/sound_button.gif' ?> onclick="document.getElementById(<?php echo $id_soal; ?>).play();"> <br>

                        <?php }
                        ?>    


                    </td>
                </tr>

                </tr>

                <tr>
                    <td >&nbsp;</td>
                    <td><input name="pilihan[<?php echo $id_soal; ?>]" type="radio" value="A"> <?php echo "$jawaban_a"; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>

                    <td><input name="pilihan[<?php echo $id_soal; ?>]" type="radio" value="B"> <?php echo "$jawaban_b"; ?> </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input name="pilihan[<?php echo $id_soal; ?>]" type="radio" value="C"> <?php echo "$jawaban_c"; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input name="pilihan[<?php echo $id_soal; ?>]" type="radio" value="D"> <?php echo "$jawaban_d"; ?></td>
                </tr>

            </table>
        <?php endforeach; ?>

        <div style="padding-top: 10px; padding-bottom:10px; float: right; ">
            <td>&nbsp;</td>
<!--                <td><input type="submit" name="submit" value="Jawab" ></td>-->

            <input type="submit" name="submit" value="Jawab" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')">
        </div>

        <?php echo form_close() ?>

    </div>
</div>
